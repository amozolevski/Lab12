package StrLengthTest;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static StrLength.Application.calcStringLength;

public class ApplicationTest {

    @Test
    public void calcStringLengthTestCase(){
        String s = "qwerty0123456789";
        int result = calcStringLength(s);
        Assert.assertTrue(result <= 10, "String length " + s + " > 10");
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void calcStringLengthExcTestCase(){
        int result = calcStringLength("");
        Assert.assertTrue(result > 0);
    }

    @DataProvider(name = "testStringLength")
    public Object[][] createCalcStringLengthTest(){
        return new Object[][]{{"qwertyuio"},
                            {"asdfghjk"},
                            {"zxcvbnmkljhgf"},
                            {"qazwsxedc"}
        };
    }

    @Test(dataProvider = "testStringLength")
    public void testCalcStringLengthTestCase(String s){
        int result = calcStringLength(s);
        Assert.assertTrue(result <= 10, "String length " + s + " > 10");
    }
}
