package JunitTests;

import Animals.Cat;
import Figures.Figure;
import Figures.Square;
import org.junit.Before;
import org.junit.Test;
import org.testng.Assert;

public class TestApplication {
    Figure figure;
    Cat cat;

    @Before
    public void testCreateFigure(){
        figure = new Square(4);
        cat = new Cat();
    }

    @Test
    public void testMakeSound(){
        String expected = "Meow! Meow!";
        String result = cat.makeSound();
        Assert.assertEquals(expected, result, "message isn't Meow! Meow!");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFigure(){
        figure = new Square(-4);
    }

    @Test
    public void testGetPerimeter(){
        Double expected = 15.0;
        Double result = figure.getPerimeter();
        Assert.assertEquals(expected, result, "wrong perimeter");
    }
}
