/*
Один интерфейс(Animal) и два наследника(Cat & Dog)
 */

package Animals;

public class Application {
    public static void main(String[] args){
        System.out.println(new Cat().makeSound());
        System.out.println(new Dog().makeSound());
    }
}
